$(document).ready(function() {
  
  var url = 'http://gdata.youtube.com/feeds/api/videos?author=dedilhadas&orderby=published&max-results=10&v=2&alt=json';
  var items = [];
  var classes = ['error', 'warning', 'info', 'success'];
  var count_class = 0;
  var src = "https://www.youtube.com/v/"
  var video = "<object>"
            + "<embed src='src_to_replace'"
            + " type='application/x-shockwave-flash'"
            + " allowfullscreen='false'"
            + " allowScriptAccess='always'"
            + " width='100%'"
            + " height='360'>"
            + "</embed>"
            + "</object>";
    
  $.getJSON(url, function(data) {
    $.each(data.feed.entry, function(key, val) {
     
      var id = val.id.$t.split(':')[3];
      var title = val.title.$t;
      
      items.push('<div class="alert alert-' + classes[count_class] + '" id="' + id + '"><i class="icon-youtube-play"></i> ' + title + '</div>');
      
      count_class++;
      
      if(count_class > classes.length - 1){
        count_class = 0;
      }
    });
        
    $('#items').html(items.join(''));
    
    create_video_embed($('.alert').first().attr('id'));
  });
  
  $(document).bind("ajaxStart.mine", function() {
    $('#spinner').show();
  });

  $(document).bind("ajaxStop.mine", function() {
    $('#spinner').hide();
    $('.alert').click(function(){
      create_video_embed($(this).attr('id'))
    });
  });
  
  function create_video_embed(video_id) {
      var src_complete = src + video_id;
      var video_complete = video.replace('src_to_replace', src_complete);
      $('#player').html(video_complete);
      $('#player').show();
  }
});

/*
GET VIDEOS BY AUTHOR v2
http://gdata.youtube.com/feeds/api/videos?author=dedilhadas
                                         &orderby=published
                                         &max-results=10
                                         &v=2
                                         &alt=json

API Youtube v2
https://developers.google.com/youtube/2.0/developers_guide_protocol_api_query_parameters?hl=pt-BR#authorsp
*/